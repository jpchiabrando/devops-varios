 #!/bin/bash

#title           :listaction.sh
#description     :Ejecuta una accion sobre una lista de archivos de un path. Esta caso Mix de wav
#author          :JChiabrando
#creation_date   :200930
#last_update     :200930
#version         :1.0
#usage           :bash listaction.sh /path
#REF             :
#===============================================================================
#
PATH_S=$1

find $PATH_S -name \*in.wav -type f -print0 | xargs -0 -I{} echo {} | sed 's/-in.wav//' > /tmp/in.list 

while IFS= read -r line
do
  #echo "$line"
  #echo $line-in.wav $line-out.wav $line.wav
  sox -m $line-in.wav $line-out.wav $line.wav
done < /tmp/in.list

exit 0