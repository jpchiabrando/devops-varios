 #!/bin/bash

#title           :deploy_cloudrun.sh
#description     :
#author          :JChiabrando
#creation_date   :210129
#last_update     :210129
#version         :1.0
#usage           :bash deploy_cloudrun.sh
#REF             :
#===============================================================================
#

#chequeo de cuenta en uso
gcloud config configurations list


#Chequeo de proyectos disponibles para el usuario y elegimos uno
#gcloud projects list 
gcloud projects list > projects &&  awk '{print $1}' projects | grep -v PROJECT_ID > project_id

c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
done < ./project_id

let c=$c+1
echo "[ new ] = Nuevo proyecto"

#elegir opcion
echo " "
echo "Elija el numero de la opcion deseado o escriba new en caso de ser un nuevo proyecto presione CRTL+C para salir"
read OPT

#Creando nuevo proyecto
if [ "$OPT" == "new" ]; then
    echo " "
    echo "Nombre del nuevo proyecto"
    echo " "  
    read PROJECT_ID
    gcloud projects create $PROJECT_ID
fi


#Si se elige un proyecto que existe
echo " "
echo "Eligio el proyecto:"
awk "NR==$OPT" project_id

echo " "
echo "Es la opcion deseado? escriba si para continuar o cualquier caracter para salir"
read CONTINUAR
if [ "$CONTINUAR" != "si" ]; then
    echo " "
    echo "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script"
    exit 0
fi

PROJECT_ID=$(awk "NR==$OPT" project_id)

echo PROJECT_ID=$PROJECT_ID > deploy_vars

gcloud config set project $PROJECT_ID

# #Listar regiones disponibles y seleccionar una
gcloud run regions list | grep -v NAME > regions

echo " "
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
done < ./regions

#elegir opcion de regiones
echo " "
echo "Elija el numero de la opcion deseado"
read OPT2

echo " "
echo "Eligio la region:"
awk "NR==$OPT2" regions

echo " "
echo "Es la opcion deseado? escriba si para continuar o cualquier caracter para salir"
read CONTINUAR
if [ "$CONTINUAR" != "si" ]; then
    echo " "
    echo "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script"
    exit 0
fi

REGION=$(awk "NR==$OPT2" regions)

echo REGION=$REGION >> deploy_vars


#Selecionar imagen y tag
gcloud container images list | grep -v NAME > images

echo " "
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
  gcloud container images list-tags $line
  echo " "
done < ./images

#elegir opcion de regiones
echo " "
echo "Elija la imagen a utilizar"
read OPT3

echo " "
echo "Eligio la imagen:"
awk "NR==$OPT3" images

echo " "
echo "Elija el tag correpondient a la imagen a utilizar, en caso de estar mal escrito dara error el proceso"
read TAG

IMAGE=$(awk "NR==$OPT3" images)


echo " "
echo "La imagen tag es la seleccionada:"
echo $IMAGE:$TAG
echo " "
echo "Es la opcion deseado? escriba si para continuar o cualquier caracter para salir"
read CONTINUAR
if [ "$CONTINUAR" != "si" ]; then
    echo " "
    echo "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script"
    exit 0
fi

echo IMAGE=$IMAGE:$TAG>> deploy_vars





# #listar variables de entorno
# read ARCHIVO env 



# #Correr el ploy
# gcloud builds submit