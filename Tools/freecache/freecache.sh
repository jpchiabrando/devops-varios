 #!/bin/bash

#title           :freecache.sh
#description     :Esta tool limpia el cahche en caso de ser necesario
#author          :JChiabrando
#creation_date   :200924
#last_update     :200924
#version         :1.1
#usage           :bash freecache_v10.sh
#REF             :
#===============================================================================

TOTAL=$(grep MemTotal /proc/meminfo | awk '{print $2 / 1024}')

FREE=$(grep Free /proc/meminfo | awk '{print $2 / 1024}' | head -1)

CACHED=$(grep Cached /proc/meminfo | awk '{print $2 / 1024}' | head -1)

USO=$(bc <<< "scale=4; $CACHED / $TOTAL *100")

PORCENT=35

if (( $(echo $USO'>'$PORCENT | bc -l) )); 
then
    echo "-----------------------------------------------" >> /var/log/freecache.log
    echo "$(date +'<%Y-%m-%d %H:%M:%S>') -- Run freecache" >> /var/log/freecache.log
    echo "Memoria Total= $TOTAL MB " >> /var/log/freecache.log
    echo "Memoria Cache= $CACHED MB " >> /var/log/freecache.log
    echo "Memoria Libre= $FREE MB " >> /var/log/freecache.log
    echo "<-Run freecache->" >> /var/log/freecache.log
    sync; echo 1 > /proc/sys/vm/drop_caches
    sleep 5
    FREE=$(grep Free /proc/meminfo | awk '{print $2 / 1024}' | head -1)
    echo "Memoria Libre= $FREE MB " >> /var/log/freecache.log
    exit 0
fi
exit 0

