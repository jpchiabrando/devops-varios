 #!/bin/bash

#title           :IncronSync
#description     :Este scrip se ejecuta para sincronizar una carpta y subcarpetas de sistemas distintos (VM,server,etc)
#author          :JChiabrando
#creation_date   :200610
#last_update     :200616
#version         :1.2
#usage           :bash IncronSync_v1.2 PATH_sync
#REF             :
#===============================================================================
 #!/bin/bash
PATH_S=$1

#--- Getting Start ---
#Leyendo configuraciones de /etc/IncronSync/IncronSync.conf
source <(grep = /etc/IncronSync/IncronSync.conf | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')

#Sincronizando desde Servidor Secundario a Primario a Secundario
#echo "$(date +'<%Y-%m-%d %H:%M:%S>') -- Sever Primary <- Server Secondary" >> /var/log/IncronSync.log

#Sincronizando desde Servidor Primario a Secundario
echo "$(date +'<%Y-%m-%d %H:%M:%S>') -- Sever Primary -> Server Secondary" >> /var/log/IncronSync.log

echo $PASS | sudo -S rsync -auv $PATH_S/ $USER@$IP:$PATH_S  >> /var/log/IncronSync.log

#Sincronizando desde Servidor Secundario a Primario a Secundario
echo "$(date +'<%Y-%m-%d %H:%M:%S>') -- Sever Primary <- Server Secondary" >> /var/log/IncronSync.log

echo $PASS | sudo -S rsync -auv $USER@$IP:$PATH_S/ $PATH_S >> /var/log/IncronSync.log


echo "-----------------------------------------------------------" >> /var/log/IncronSync.log
