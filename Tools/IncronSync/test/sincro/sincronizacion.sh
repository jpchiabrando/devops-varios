 #!/bin/bash

#title           :sincronizacion.sh
#description     :Este scrip se ejecuta para sincronizar una carpta de sistemas distintos (VM,server,etc)
#author          :JChiabrando
#creation_date   :200610
#last_update     :200610
#version         :1.0
#usage           :bash sincronizacion.sh
#REF             :
#===============================================================================

function title () {
    echo ""
    echo -e "$YELLOW------------------------------------------------$NORMAL"
    echo -e " .:: $BOLD$1$NORMAL ::."
    echo -e "$YELLOW------------------------------------------------$NORMAL"
    echo ""
}

#-------------------
#LOG rotation


# ----------------------
# ----------------------
# Getting Start

#Clave de root user
PASS=globaltec..


#fecha= $(date +"<%Y-%m-%d %H:%M:%S>") 

echo "$(date +'<%Y-%m-%d %H:%M:%S>') -- Sever Primary <- Server Secondary" >> /var/log/rsync.log

echo $PASS | sudo -S rsync -avvhurOI gtting@192.168.144.244:/home/gtting/sincro/ sincro | tail -n4 >> /var/log/rsync.log

echo "$(date +'<%Y-%m-%d %H:%M:%S>') -- Sever Primary -> Server Secondary" >> /var/log/rsync.log

echo $PASS | sudo -S rsync -avvhurOI sincro/ gtting@192.168.144.244:/home/gtting/sincro | tail -n4 >> /var/log/rsync.log

echo "-----------------------------------------------------------" >> /var/log/rsync.log
