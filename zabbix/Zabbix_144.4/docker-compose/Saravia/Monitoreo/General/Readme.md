# Monitoreo General
Implementación de servidor Zabbix + Grafana

Para ejecutarlo será necesario ingresar a la carpeta ./docker y ejecutar el comando

docker-compose up -d

Esto descargará los docker necesarios para Zabbix y construirá e instalará el correspondiente a Grafana

## Zabbix
El puerto expuesto es el 8080, por lo que para su administración se deberá ingresar a http://localhost:8080 utilizando Admin | zabbix

## Grafana
El puerto expuesto es el 3000, por lo que para su administración se deberá ingresar a http://localhost:3000 utilizando admin | admin

### Cambio de logo
Dentro de la carpeta ./grafana se encuentra el dockerfile que personaliza los logos de Grafana, éstos pueden ser reemplazados por los logos de: Denwa o de GlobalThink Technology, según la carpeta a la que se haga referencia, siendo:

./assets/dnw/ para Denwa
./assets/gtt/ para GlobalThink Technology 

### Plugins instalados
grafana-clock-panel
grafana-simple-json-datasource
alexanderzobnin-zabbix-app 3.12.4
agenty-flowcharting-panel 0.9.0
grafana-piechart-panel 1.6.0
yesoreyeram-boomtheme-panel 0.1.0
grafana-azure-data-explorer-datasource 3.0.5
grafana-azure-monitor-datasource 0.3.0
grafana-clock-panel 1.0.3
grafana-image-renderer 2.0.0
grafana-kubernetes-app 1.0.1
grafana-simple-json-datasource 1.4.1
grafana-strava-datasource 1.1.1
grafana-worldmap-panel 0.3.2


