#!/bin/bash

# Este script interactivo crea un script firewall_<nombre_de_la_empresa>.sh que
# al ser ejecutado crea las reglas en la cadena INPUT de iptables.
#
# TODO: 
# * poder ingresar protocolo/puerto arbitrariamente.
# Version 1.0


PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
clear

unset IPs

#IPs[0]=192.168.1.1
#IPs[1]=192.168.1.2
#IPs[2]=192.168.1.3
#IPs[3]=192.168.1.4

#for i in ${IPs[@]};do
#    #echo ${IPs[$count]}
#    echo $i
#done
#exit 0

###################################################
# Variables
GTTRange="200.69.250.89-200.69.250.93"

## colors

NORMAL='\E[0;0m'
BOLD='\E[0;1m'
RED='\E[31;3m'
YELLOW='\E[33;3m'
GREEN='\E[32;3m'
BOLD_RED='\E[31;1m'
BOLD_YELLOW='\E[33;1m'
BOLD_GREEN='\E[32;1m'

###################################################
## Funciones

function salida () {
    case $1 in
    1 )
        TYPE="ERROR"
        TYPE_COLOR=$BOLD_RED
        LETTER_COLOR=$NORMAL
#       LETTER_COLOR=$RED
        ;;
    2 )
        TYPE="WARNING"
        TYPE_COLOR=$BOLD_YELLOW
        LETTER_COLOR=$NORMAL
#       LETTER_COLOR=$YELLOW
        ;;
    3 )
        TYPE="INFO"
        TYPE_COLOR=$BOLD_GREEN
        LETTER_COLOR=$NORMAL
#       LETTER_COLOR=$GREEN
        ;;
    4 )
        TYPE="ErrINFO" # Explicacion
        TYPE_COLOR=$BOLD_YELLOW
        LETTER_COLOR=$NORMAL
#       LETTER_COLOR=$YELLOW
        ;;
    5 )
        TYPE="????" # Pregunta
        TYPE_COLOR=$BOLD_YELLOW
        LETTER_COLOR=$NORMAL
#       LETTER_COLOR=$BOLD
        ;;
    esac
    
    echo -e $3 "$TYPE_COLOR$TYPE$NORMAL: $LETTER_COLOR$2$NORMAL"

}

function title () {
    echo ""
    echo -e "$YELLOW----------------------------$NORMAL"
    echo -e " .:: $BOLD$1$NORMAL ::."
    echo -e "$YELLOW----------------------------$NORMAL"
    echo ""
}

function subtitle () {
    echo ""
    echo -e " -- $BOLD$1$NORMAL --"
    echo ""
}

function enter_range() {
    while true;do
        salida "3" "Please, enter the range of IP addresses:"
        salida "5" "   IP from: " "-n"
        read IP_FROM
        salida "5" "     IP to: " "-n"
        read IP_TO
        echo
        salida "3" " IP Range: [$BOLD$IP_FROM - $IP_TO$NORMAL]"
        salida "5" "$BOLD Is this OK?$NORMAL [y/N] " "-n"
        read OK
        if [ "$OK" == "y" -o "$OK" == "Y" ];then break ;fi
    done
    IPRange="$IP_FROM-$IP_TO"
}

function enter_IP() {
    # Si ya se setearon IPs addresses anteriormente
    echo
    if [ ${#IPs[@]} -ne 0 ];then
        salida "5" "Do you want to use the following IP addresses:"
        echo
        echo -e "IP number | IP address"
        echo "----------+------------"
        count=0
        for i in ${IPs[@]};do
            printf "   %3d    | %s\n" $(($count+1)) ${IPs[$count]}
            count=$(($count+1))
        done
        echo
        salida "5" "$BOLD do you?$NORMAL [y/n]: " "-n"
        read USE_BEFORE
    fi
    if [ ${#IPs[@]} -eq 0 -o "$USE_BEFORE" == "n" -o "$USE_BEFORE" == "N" ] ;then
        unset IPs
        READY="no"
        while [ "$READY" == "no" ];do
            IPCount=0
            STOP="no"
            while [ "$STOP" != "yes" ];do
                salida "5" "Enter IP address "$(($IPCount+1))" (\"end\" to finish): " "-n"
                read ENTER_IP
                if [ "$ENTER_IP" == "end" ];then
                    STOP="yes"
                else
                    IPs[$IPCount]=$ENTER_IP
                fi
                IPCount=$(($IPCount+1))
            done

            echo
            echo -e "IP number | IP address"
            echo "----------+------------"
            count=0
            for i in ${IPs[@]};do
                printf "   %3d    | %s\n" $(($count+1)) ${IPs[$count]}
                count=$(($count+1))
            done
            echo
            ask_for_yes_or_no "Is this OK"
            is_this_ok=$?
            if [ $is_this_ok -eq 1 ]; then READY="yes";
            else STOP="no"; unset IPs; fi
        done
    fi
    # poner las IP en un formato que se puedan poner directamente en el comando iptables (o no, ja!)
}

function enter_port() {
    service=$1
    def_port=$2
    if [ "x.$def_port" == "x." ];then def_port_str="";
    else def_port_str="(ENTER for default: $def_port)"; fi
    while true ; do
        salida "5" " Enter $service port $def_port_str: " "-n"
        read ENTER_PORT
        if [ "x.$def_port" != "x." ] && [ "x.$ENTER_PORT" == "x." ];then ENTER_PORT=$def_port; fi
        salida "5" " Re-enter $service port $def_port_str: " "-n"
        read ENTER_PORT_RE
        if [ "x.$def_port" != "x." ] && [ "x.$ENTER_PORT_RE" == "x." ];then ENTER_PORT_RE=$def_port; fi
        if [ "$ENTER_PORT" == "$ENTER_PORT_RE" ];then
            break;
        else
            salida "2" "Ports do not match!\n"
        fi
    done
    PORT=$ENTER_PORT
}

function to_file () {
    if [ "$FILENAME.x" != ".x" ];then
        echo $1 >> $FILENAME
        if [ "$2.x" == "n.x" ];then echo "" >> $FILENAME ;fi
    fi
}

function ask_for_yes_or_no () {
#Usage example:
#ask_for_yes_or_no "Angel es uto" "yes"
#angel_uto=$?
#echo $angel_uto
# return values:
# 0: no
# 1: yes
# 2: error (no question)
    question=$1
    default=$2
    if [ "x.$question" == "x." ];then return 2; fi
    if [ "x.$default" == "x.no" ];then default_str="[y/N]"; default_case="no";
    elif [ "x.$default" == "x.yes" ];then default_str="[Y/n]"; default_case="yes";
    else default_str="[y/n]"; default_case=""; fi
    while true;do
        echo
        salida "5" "${question}? ${default_str}: " "-n"
        read FLUSH_ALL
        case $FLUSH_ALL in
        "y" | "Y")
            return 1
            break
            ;;
        "n" | "N")
            return 0
            break
            ;;
        *)
            if [ "$default_case" == "" ];then salida "2" "Bad option. Please choose \"y\" or \"n\".";
            elif [ "$default_case" == "yes" ]; then return 1; break;
            else return 0; break; fi
            ;;
        esac
    done
}

function generate_rule () {
    rule=$1
    rule=`echo $rule | sed -r "s/\{%gttrange%\}/$GTTRange/g"`
    rule=`echo $rule | sed -r "s/\{%protocol%\}/$PROTOCOL/g"`
    rule=`echo $rule | sed -r "s/\{%port_str%\}/$PORT_STR/g"`
    rule=`echo $rule | sed -r "s/\{%ip%\}/$IP/g"`
    rule=`echo $rule | sed -r "s/\{%iprange%\}/$IPRange/g"`
    echo $rule
}

function enter_file () {
    while true ; do
        salida "5" " Enter file with IP addresses: " "-n"
        read ENTER_FILE
        if [ "x.$ENTER_FILE" != "x." ] && [ -f $ENTER_FILE ];then
            IP_FILE=$ENTER_FILE
            break;
        else
            salida "2" "File $ENTER_FILE does not exist!\n"
        fi
    done
}

###################################################
# Starting

title "Firewall"

salida "3" "Enter Company Name: " "-n"
read COMPANY

if [ -z "`echo $COMPANY | grep -v grep | grep "/"`" ];then
    FILENAME="firewall_"`echo $COMPANY | sed -r "s/\s+/_/g"`".sh"
elif [ -z "`echo $COMPANY | grep -v grep | grep "#"`" ];then
    FILENAME="firewall_"`echo $COMPANY | sed -r "s#\s+#_#g"`".sh"
else
    FILENAME="firewall_file.sh"
fi

rm -f $FILENAME
touch $FILENAME

to_file "#!/bin/bash" "n"
chmod +x $FILENAME

to_file "# Loading modules ..."
to_file "/sbin/modprobe ip_tables"
to_file "/sbin/modprobe iptable_nat"
to_file "/sbin/modprobe iptable_filter" "n"

# Flush all chains
title "Flushing iptables"
salida "3" "$BOLD This are the iptables now:$NORMAL\n"
/sbin/iptables -L -v -n
ask_for_yes_or_no "Do you want to FLUSH all iptables" "yes"
flush_iptables=$?
case $flush_iptables in
1)
    salida "3" "Flushing all iptables ..."
    to_file "# Flushing all iptables ..."
    to_file "/sbin/iptables -P INPUT ACCEPT"
    to_file "/sbin/iptables -P OUTPUT ACCEPT"
    to_file "/sbin/iptables -P FORWARD ACCEPT"
    to_file "/sbin/iptables --flush"
    to_file "/sbin/iptables -t nat --flush"
    to_file "/sbin/iptables -X LOGDROP 2> /dev/null" "n" # Si existe la chain LOGDROP la borramos.
    ;;
0)
    echo "Not flushing iptables"
    ;;
esac
echo

# Drop ICMP echo-request messages sent to broadcast or multicast addresses
salida "3" "Drop ICMP echo-request messages sent to broadcast or multicast addresses"
to_file "# Drop ICMP echo-request messages sent to broadcast or multicast addresses"
to_file "echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts" "n"

# Drop source routed packets
salida "3" "Drop source routed packets"
to_file "# Drop source routed packets"
to_file "echo 0 > /proc/sys/net/ipv4/conf/all/accept_source_route" "n"

# Enable TCP SYN cookie protection from SYN floods
salida "3" "Enable TCP SYN cookie protection from SYN floods"
to_file "# Enable TCP SYN cookie protection from SYN floods"
to_file "echo 1 > /proc/sys/net/ipv4/tcp_syncookies" "n"

# Don't accept ICMP redirect messages
salida "3" "Don't accept ICMP redirect messages"
to_file "# Don't accept ICMP redirect messages"
to_file "echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects" "n"

# Don't send ICMP redirect messages
salida "3" "Don't send ICMP redirect messages"
to_file "# Don't send ICMP redirect messages"
to_file "echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects" "n"

# Enable source address spoofing protection
salida "3" "Enable source address spoofing protection"
to_file "# Enable source address spoofing protection"
to_file "echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter" "n"

# Log packets with impossible source addresses
salida "3" "Log packets with impossible source addresses"
to_file "# Log packets with impossible source addresses"
to_file "echo 1 > /proc/sys/net/ipv4/conf/all/log_martians" "n"

# Allow unlimited traffic on the loopback interface
echo
salida "3" "Allow unlimited traffic on the loopback interface ..."
to_file "# Allow unlimited traffic on the loopback interface"
to_file "/sbin/iptables -A INPUT -i lo -j ACCEPT"
to_file "/sbin/iptables -A OUTPUT -o lo -j ACCEPT"
to_file "/sbin/iptables -A INPUT -s localhost -j ACCEPT" "n"

# Previously initiated and accepted exchanges bypass rule checking
# Allow unlimited outbound traffic
echo
salida "3" "Allowing ESTABLISHED and RELATED connections"
ask_for_yes_or_no "Do you want to allow already ESTABLISHED or RELATED connections" "yes"
allow_established=$?
case $allow_established in
1)
    salida "3" "Allow already ESTABLISHED or RELATED connections ..."
    to_file "# Previously initiated and accepted exchanges bypass rule checking"
    to_file "# Allow unlimited outbound traffic"
    to_file "/sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT"
    to_file "/sbin/iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT" "n"
    ;;
0)
    echo "Not allowing already ESTABLISHED and/or RELATED connections"
    ;;
esac
echo

######################################################################################################

###################################################
# ------------------  SERVICES  ------------------

# Accept ssh incoming traffic from 192.168.1.38 (para que funcione desde mi maquina, je)
#echo
#echo "Accept ssh incoming traffic from 192.168.1.38 (para que funcione desde mi maquina, je)"
#/sbin/iptables -A INPUT -s 192.168.1.38 -p tcp --dport 22 -m state --state NEW -j ACCEPT

to_file "# ------------------  SERVICES  -------------------"
serv_names=( "SSH" "MySQL" "PING" "YTone" "HTTP" "HTTPS" "SIP (5060)" "SIP (5061)" "SIP (5065)" "RTP" "MGCP" "DNS" )
serv_qtty=${#serv_names[@]}
serv_protocol=( "tcp" "tcp" "icmp" "tcp" "tcp" "tcp" "udp" "udp" "udp" "udp" "udp" "udp" )
serv_def_ports=( "22" "3306" "no" "5038:5039" "80" "443" "5060" "5061" "5065" "10000:20000" "2727" "53" )

serv_rule_option1='/sbin/iptables -A INPUT -m iprange --src-range {%gttrange%} -p {%protocol%} {%port_str%} -j ACCEPT'
serv_rule_option2='/sbin/iptables -A INPUT -s {%ip%} -p {%protocol%} {%port_str%} -j ACCEPT'
serv_rule_option3='/sbin/iptables -A INPUT -m iprange --src-range {%iprange%} -p {%protocol%} {%port_str%} -j ACCEPT'
serv_rule_option4='/sbin/iptables -A INPUT -s {%ip%} -p {%protocol%} {%port_str%} -j ACCEPT'
serv_rule_option5='/sbin/iptables -A INPUT -p {%protocol%} {%port_str%} -j ACCEPT'

# PING
#    '/sbin/iptables -A INPUT -m iprange --src-range {%gttrange%} -p icmp -j ACCEPT'
#    '/sbin/iptables -A INPUT -s {%ip%} -p icmp -j ACCEPT'
#    '/sbin/iptables -A INPUT -m iprange --src-range {%iprange%} -p icmp -j ACCEPT'
#    '/sbin/iptables -A INPUT -p icmp -j ACCEPT'


#echo `printf "$format" "$GTTRange" "$PORT"`

#PROTOCOL="${serv_protocol[0]}"
#enter_port "${serv_names[0]}" "${serv_def_ports[0]}"
#echo `generate_rule "${serv_rule_option1}"`

title "SERVICES"
for serv in `seq 0 $((serv_qtty-1))`; do
    subtitle "${serv_names[$serv]}"
    PROTOCOL="${serv_protocol[$serv]}"
    if [ "${serv_def_ports[$serv]}" == "no" ]; then
        PORT_STR="";
    else
        enter_port "${serv_names[$serv]}" "${serv_def_ports[$serv]}"
        PORT_STR="--dport $PORT";
    fi
    echo ""
    KEEP_ROLLING=true
    while true ;do
        echo ""
        salida "3" "Accept ${serv_names[$serv]} from: "
        echo -e "$BOLD(1)$NORMAL GTT"
        echo -e "$BOLD(2)$NORMAL Some IP addresses (entered manually)"
        echo -e "$BOLD(3)$NORMAL Range of IP address (entered manually)"
        echo -e "$BOLD(4)$NORMAL Some IP addresses (from file)"
        echo -e "$BOLD(5)$NORMAL Anywhere"
        echo -e "$BOLD(6)$NORMAL Do not set any rule for this service"
        echo -e "$BOLD(7)$NORMAL Done (continue with next Service)."
        salida "5" "Option: " "-n"
        read FROM_OPTION
        case $FROM_OPTION in
        1) # From: GTT only
            to_file "# Accepting ${serv_names[$serv]} on port $PORT from GTT ..."
            iptables_rule1=`generate_rule "${serv_rule_option1}"`
            to_file "$iptables_rule1" "n"
            salida "3" "Accepting$BOLD ${serv_names[$serv]}$NORMAL on port $PORT from GTT ..."
            ;;

        2) # From: Some IP addresses (entered manually)
            to_file "# Accepting ${serv_names[$serv]} on port $PORT from IP address list ..."
            enter_IP
            for ip in ${IPs[@]};do
                IP="$ip"
                iptables_rule2=`generate_rule "${serv_rule_option2}"`
                to_file "$iptables_rule2"
            done
            to_file ""
            salida "3" "Accepting$BOLD ${serv_names[$serv]}$NORMAL on port $PORT from IP address list ..."
            ;;

        3) # From: Range of IP addresses (entered manually)
            to_file "# Accepting ${serv_names[$serv]} on port $PORT from Range of IP address ..."
            enter_range
            iptables_rule3=`generate_rule "${serv_rule_option3}"`
            to_file "$iptables_rule3" "n"
            salida "3" "Accepting$BOLD ${serv_names[$serv]}$NORMAL on port $PORT from Range of IP address ..."
            ;;

        4) # From: Some IP addresses (from file)
            to_file "# Accepting ${serv_names[$serv]} on port $PORT from IP address list (from file) ..."
            enter_file
            while read line;do
                IP="$line"
                if [ "$line" == "" ] || [ "`echo $line | cut -c 1`" == "#" ];then continue; fi
                iptables_rule4=`generate_rule "${serv_rule_option4}"`
                to_file "$iptables_rule4"
            done < $IP_FILE
            to_file ""
            salida "3" "Accepting$BOLD ${serv_names[$serv]}$NORMAL on port $PORT from IP address list (from file) ..."
            ;;

        5) # From: Anywhere
            to_file "# Accepting ${serv_names[$serv]} on port $PORT from anywhere ..."
            iptables_rule5=`generate_rule "${serv_rule_option5}"`
            to_file "$iptables_rule5" "n"
            salida "3" "Accepting$BOLD ${serv_names[$serv]}$NORMAL on port $PORT from anywhere ..."
            ;;

        6) # From: Do nothing
            to_file "# Do nothing with service ${serv_names[$serv]} ..." "n"
            KEEP_ROLLING=false
            break
            ;;

        7) # Done (continue with next Service).
            KEEP_ROLLING=false
            break
            ;;

        *) # Bad option, choose again
            echo ""
            salida "2" "Bad option. Please choose 1, 2, 3, 4, 5, 6 or 7."
            ;;
        esac
    done
done

###################################################
# ------------------  POLICIES  ------------------

chains=( "INPUT" "OUTPUT" "FORWARD" )
chains_qtty=${#chains[@]}
chain_def_policies=( "DROP" "ACCEPT" "DROP" )

to_file "# ------------------  POLICIES  -------------------"
title "Setting Policies"
for pol in `seq 0 $((chains_qtty-1))`; do
    KEEP_ROLLING=true
    while $KEEP_ROLLING ;do
        if [ ${chain_def_policies[$pol]} == "ACCEPT" ];then default_accept="(default)"; default_drop="";
        else default_accept=""; default_drop="(default)"; fi
        salida "3" "Set policy for chain ${chains[$pol]} to: "
        echo -e "$BOLD(1)$NORMAL ACCEPT $default_accept"
        echo -e "$BOLD(2)$NORMAL DROP $default_drop"
        salida "5" "Option [1/2]?: " "-n"
        read POL_OPTION
        case $POL_OPTION in
        
        1) # ACCEPT
            to_file "# Chain ${chains[$pol]} -> poicy = ACCEPT"
            to_file "/sbin/iptables --policy ${chains[$pol]} ACCEPT" "n"
            salida "3" "Setting the$BOLD ${chains[$pol]}$NORMAL chain policy to$BOLD ACCEPT$NORMAL ..."
            KEEP_ROLLING=false
            break
            ;;
            
        2) # DROP
            to_file "# Chain ${chains[$pol]} -> poicy = DROP"
            to_file "/sbin/iptables --policy ${chains[$pol]} DROP" "n"
            salida "3" "Setting the$BOLD ${chains[$pol]}$NORMAL chain policy to$BOLD DROP$NORMAL ..."
            KEEP_ROLLING=false
            break
            ;;

        *) # Bad option, choose again
            echo ""
            salida "2" "Bad option. Please choose 1 or 2."
            ;;
        esac
    done
done

###################################################
# ------------------  FINISH  ------------------
title "Finishing ..."
# See the script with the rules?
ask_for_yes_or_no "Do you want to see the script with the iptables rules" "yes"
see_iptables_file=$?
if [ $see_iptables_file -eq 1 ];then
    echo
    cat $FILENAME | more
    echo
fi

ask_for_yes_or_no "Do you want to$BOLD apply$NORMAL the script with the iptables rules now" "no"
apply_iptables_rules=$?
if [ $apply_iptables_rules -eq 1 ];then
    salida "3" "$BOLD Applying iptables ...$NORMAL"
    ./$FILENAME
fi

OS="centos"  # assume Centos
if [ -f /etc/debian_version ]; then
    OS="ubuntu"
fi
if [ -f /etc/gentoo-release ]; then
    OS="ubuntu"
fi

if [ "$OS" == "ubuntu" ]; then
    ask_for_yes_or_no "Do you want to$BOLD execute$NORMAL the iptables script every$BOLD system restart$NORMAL" "yes"
    execute_at_boot=$?
    if [ $execute_at_boot -eq 1 ];then
        cp $FILENAME /root
        chmod a+x /root/$FILENAME
        if [ `cat /etc/rc.local | grep -v "^#" | grep -e "/root/$FILENAME" --count` -eq 0 ];then
            if [ `cat /etc/rc.local | grep -v "^#" | grep -e "^exit 0" --count` -gt 0 ];then
                sed -i "s#^exit 0#/root/$FILENAME 2> /dev/null \nexit 0#g" /etc/rc.local
            else
                echo "/root/$FILENAME 2> /dev/null \nexit 0" >> /etc/rc.local
            fi
        fi
    fi
fi

salida "3" "The script with all iptables rules is: $FILENAME"
echo ""
echo -e "\n$BOLD Bye.$NORMAL\n"
