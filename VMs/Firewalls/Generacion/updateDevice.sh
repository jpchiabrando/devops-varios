#!/bin/bash

HERE=$(pwd)

cd /usr/local/dendown/devices/

manufCount=0
declare -a manufs
echo ""
echo " -- Choose manufacturer -- "
for manuf in $(ls -d */);do
    manufs[$manufCount]=$manuf
    echo "($manufCount) - $manuf"
    manufCount=$(($manufCount+1))
done
echo -n "???  "
read MANUF_INDEX
if [ ! -d "${manufs[$MANUF_INDEX]}" ];then
    echo "Bad choice"
    exit 0
fi
cd ${manufs[$MANUF_INDEX]}

modelCount=0
declare -a models
echo ""
echo " -- Choose model -- "
for model in $(ls -d */);do
    models[$modelCount]=$model
    echo "($modelCount) - $model"
    modelCount=$(($modelCount+1))
done
echo -n "???  "
read MODEL_INDEX
if [ ! -d "${models[$MODEL_INDEX]}" ];then
    echo "Bad choice"
    exit 0
fi
cd ${models[$MODEL_INDEX]}

echo ""
echo "${manufs[$MANUF_INDEX]}${models[$MODEL_INDEX]}"
echo ""

echo ""
echo "Preparing..."
rm -rf device/
rm -rf device.7z
mkdir device

echo ""
OLD_TS=$(egrep \"ts\":[0-9]* data.json | grep -o "[0-9]*")
UPDATE_TS=$(date +%s)
echo "Updating timestamp ($OLD_TS -> $UPDATE_TS)..."
sed -r -i "s/\"ts\":[0-9]*/\"ts\":$UPDATE_TS/g" data.json

cp data.json device

for i in $(find ./ -maxdepth 1 -type f);do
    [ "$i" == "./data.json" ] && continue
    ofDate=$(ls -l $i | awk '{print $6 " " $7}')
    echo ""
    echo -n "Include this file: $i ($ofDate)? [Y/n] "
    read -n 1 INCLUDE
    if [ $(echo $INCLUDE | grep -i "n" --count) -eq 0 ];then
            cp $i device/
            echo "$i included!"
    fi
done

echo ""
echo "Compressing..."
7z a -pUbD4te63v1ce5 -t7z -mx3 ./device.7z ./device

echo ""
echo "Updating file size..."
UPDATE_SIZE=$(ls -sl device.7z |awk '{print $1}')
sed -r -i "s/\"update_size\":[0-9]*/\"update_size\":$UPDATE_SIZE/g" data.json

echo ""
echo "Cleaning up..."
rm -rf device
chown www-data:root *

cd $HERE

echo ""
echo "Done!"

exit 0

#ls
#mkdir device
#cp ... device
#7z a -pUbD4te63v1ce5 -t7z -mx3 ./device.7z ./device
#rm -rf device
#chown www-data:root *
#ls -aslh

#UPDATE_SIZE=$(ls -sl device.7z |awk '{print $1}')
#sed -r -i "s/\"update_size\":[0-9]*/\"update_size\":$UPDATE_SIZE/g" data.json


