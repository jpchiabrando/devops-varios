#!/bin/bash

# Loading modules ...
# /sbin/modprobe ip_tables
# /sbin/modprobe iptable_nat
# /sbin/modprobe iptable_filter

# Flushing all iptables ...
# /sbin/iptables -P INPUT ACCEPT
# /sbin/iptables -P OUTPUT ACCEPT
# /sbin/iptables -P FORWARD ACCEPT
# /sbin/iptables --flush
# /sbin/iptables -t nat --flush
# /sbin/iptables -X LOGDROP 2> /dev/null

# Drop ICMP echo-request messages sent to broadcast or multicast addresses
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts

# Drop source routed packets
echo 0 > /proc/sys/net/ipv4/conf/all/accept_source_route

# Enable TCP SYN cookie protection from SYN floods
echo 1 > /proc/sys/net/ipv4/tcp_syncookies

# Don't accept ICMP redirect messages
echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects

# Don't send ICMP redirect messages
echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects

# Enable source address spoofing protection
echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter

# Log packets with impossible source addresses
echo 1 > /proc/sys/net/ipv4/conf/all/log_martians

# Allow unlimited traffic on the loopback interface
/sbin/iptables -A INPUT -i lo -j ACCEPT
/sbin/iptables -A OUTPUT -o lo -j ACCEPT
/sbin/iptables -A INPUT -s localhost -j ACCEPT

# Previously initiated and accepted exchanges bypass rule checking
# Allow unlimited outbound traffic
# /sbin/iptables -A INPUT -p tcp -m state --state NEW -m limit --limit 60/second --limit-burst 20 -j ACCEPT
# /sbin/iptables -A INPUT –p tcp –m state --state NEW –j DROP
/sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

# GTT
/sbin/iptables -A INPUT -s 199.204.135.104/29 -j ACCEPT
/sbin/iptables -A INPUT -s 200.61.190.80/28 -j ACCEPT
/sbin/iptables -A INPUT -s 208.87.0.72/29 -j ACCEPT
/sbin/iptables -A INPUT -s 208.87.0.152/29 -j ACCEPT
/sbin/iptables -A INPUT -s 208.87.1.204/30 -j ACCEPT
/sbin/iptables -A INPUT -s 190.225.247.64/27 -j ACCEPT

#GTT Fibertel
201.231.53.35/24


# http
/sbin/iptables -A INPUT -p tcp --dport 80 -j ACCEPT

# https
/sbin/iptables -A INPUT -p tcp --dport 443 -j ACCEPT

# accept dns on port 53
/sbin/iptables -A INPUT -s 8.8.8.8 -p udp --dport 53 -j ACCEPT
/sbin/iptables -A INPUT -s 4.2.2.2 -p udp --dport 53 -j ACCEPT

# accept ntp on port 123
/sbin/iptables -A INPUT -p udp --dport 123 -j ACCEPT
/sbin/iptables -A INPUT -p tcp --dport 123 -j ACCEPT

# use unprivileged port for ntp
/sbin/iptables -t nat -A POSTROUTING -p tcp --destination-port 123 -j SNAT --to :61123
/sbin/iptables -t nat -A POSTROUTING -p udp --destination-port 123 -j SNAT --to :61123

# SSH DROP the rest in case INPUT policy ACCEPT
/sbin/iptables -A INPUT -p tcp --dport 22 -j DROP

# DROP databases
#/sbin/iptables -A INPUT -p tcp --dport 47018 -j DROP
#/sbin/iptables -A INPUT -p tcp --dport 3306 -j DROP

# DROP internal services
#/sbin/iptables -A INPUT -p tcp --dport 44455 -j DROP

# Chain INPUT -> poicy = DROP
/sbin/iptables --policy INPUT DROP

# Chain OUTPUT -> poicy = ACCEPT
# /sbin/iptables --policy OUTPUT ACCEPT

# Chain FORWARD -> poicy = DROP
# /sbin/iptables --policy FORWARD DROP